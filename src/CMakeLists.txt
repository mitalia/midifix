cmake_minimum_required(VERSION 2.8)
set(CMAKE_TOOLCHAIN_FILE ${CMAKE_SOURCE_DIR}/arduino-cmake/cmake/ArduinoToolchain.cmake)

project(midifix C CXX)

set(TOOLSPATH "${CMAKE_CURRENT_SOURCE_DIR}/../tools/")

ADD_CUSTOM_COMMAND(
    OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/instrumentnames.cpp
    COMMAND ${TOOLSPATH}/mkpgmsarr.py instrumentNames <"${CMAKE_CURRENT_SOURCE_DIR}/res/instrumentnames.txt" >"${CMAKE_CURRENT_BINARY_DIR}/instrumentnames.cpp"
    DEPENDS res/instrumentnames.txt
    )

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O2 -std=c++11")

generate_arduino_firmware(midifix
    SRCS main.cpp instrumentnames.cpp
    PORT /dev/ttyUSB0
    BOARD atmega328)
