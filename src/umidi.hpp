#ifndef UMIDI_HPP_INCLUDED
#define UMIDI_HPP_INCLUDED
#include <Stream.h>
#include <limits.h>
#include <Arduino.h>
#include "types.hpp"

struct MidiMessage
{
    byte status;
    byte data[2];

    /**
     * true if the current instance contains a valid status byte
     */
    bool validStatus() const { return status & 0x80; }

    /**
     * true if the current instance contains a valid data0 byte
     */
    bool validData0() const { return !(data[0] & 0x80); }

    /**
     * true if the current instance contains a valid data1 byte
     */
    bool validData1() const { return !(data[1] & 0x80); }

    /**
     * Helper for other accessors; interprets the two data bytes
     * as a single 14-bit word
     */
    word dataAs14BitWord() const { return data[0] | (word(data[1])<<7); }

    enum class Type : byte
    {
        Invalid=0,
        NoteOff=0x80,
        NoteOn=0x90,
        KeyAftertouch=0xa0,
        ControlChange=0xb0,
        ProgramChange=0xc0,
        ChannelAftertouch=0xd0,
        PitchBend=0xe0,
        System=0xf0
    };

    enum class SystemType : byte
    {
        Invalid=0,
        // Common messages
        SystemExclusive=0xf0,
        TimeCodeQuarterFrame=0xf1,
        SongPositionPointer=0xf2,
        SongSelect=0xf3,
        Reserved1=0xf4,
        Reserved2=0xf5,
        TuneRequest=0xf6,
        EndExclusive=0xf7,
        // Realtime messages
        Clock=0xf8,
        Reserved3=0xf9,
        Start=0xfa,
        Continue=0xfb,
        Stop=0xfc,
        Reserved4=0xfd,
        ActiveSensing=0xfe,
        Reset=0xfe
    };

    /**
     * Message type
     */
    Type type() const { return Type(status & 0xf0); }

    /**
     * Subtype (for System messages)
     */
    SystemType systemType() const { return SystemType(status); }

    /**
     * Target channel (valid only for non-system messages)
     * 0-based (MIDI channels starting from 1 is moronic)
     */
    byte channel() const { return status & 0x0f; }

    /**
     * Key (valid for NoteOff, NoteOn and KeyAftertouch)
     */
    byte key() const { return data[0]; }

    /**
     * Note velocity (valid for NoteOff, NoteOn, KeyAftertouch)
     */
    byte velocity() const { return data[1]; }

    /**
     * Channel aftertouch velocity (valid for ChannelAftertouch)
     */
    byte channelAftertouchVelocity() const { return data[0]; }

    /**
     * Controller # (valid for ProgramChange)
     */
    byte controller() const { return data[0]; }

    /**
     * New value for the controller (valid for ProgramChange)
     */
    byte controllerValue() const { return data[1]; }

    /**
     * New program (patch) number (valid for ProgramChange)
     */
    byte programNumber() const { return data[0]; }

    /**
     * Pitch bend value (valid for PitchBend)
     */
    word bendValue() const { return dataAs14BitWord(); }

    /**
     * MIDI Time Code message type (valid for TimeCodeQuarterFrame)
     */
    byte timeCodeMsgType() const { return data[0] >> 4; }

    /**
     * MIDI Time Code value (valid for TimeCodeQuarterFrame)
     */
    byte timeCodeValue() const { return data[0] & 0xf; }

    /**
     * Song position in MIDI beats (valid for SongPositionPointer)
     */
    word songPosition() const { return dataAs14BitWord(); }

    /**
     * Song ID to play (valid for SongSelect)
     */
    byte songID() const { return data[0]; }

    /**
     * Constructor
     */
    MidiMessage(byte status=0, byte data0=0xff, byte data1=0xff)
        : status(status)
    {
        data[0]=data0; data[1]=data1;
    }

    /**
     * Helper: constructs a note on message
     */
    static MidiMessage mkNoteOn(byte channel, byte note, byte velocity=127)
    {
        return MidiMessage(channel | byte(Type::NoteOn), note, velocity);
    }

    static MidiMessage mkNoteOff(byte channel, byte note, byte velocity=0)
    {
        return MidiMessage(channel | byte(Type::NoteOff), note, velocity);
    }

    static MidiMessage mkKeyAftertouch(byte channel, byte note, byte velocity=127)
    {
        return MidiMessage(channel | byte(Type::KeyAftertouch), note, velocity);
    }

    static MidiMessage mkControlChange(byte channel, byte controller, byte value)
    {
        return MidiMessage(channel | byte(Type::ControlChange), controller, value);
    }

    static MidiMessage mkProgramChange(byte channel, byte programNumber)
    {
        return MidiMessage(channel | byte(Type::ProgramChange), programNumber);
    }

    static MidiMessage mkChannelAftertouch(byte channel, byte velocity)
    {
        return MidiMessage(channel | byte(Type::ChannelAftertouch), velocity);
    }

    static MidiMessage mkPitchBend(byte channel, word value)
    {
        return MidiMessage(channel | byte(Type::PitchBend), value & 0x7f, (value >> 7) & 0x7f);
    }
};

class UMidi
{
    Stream &s;
    byte inStatus;
    byte outStatus;
    int outStatusTime;
    int outStatusExpiry;

public:
    UMidi(Stream &s, int outStatusExpiry=1000)
        : s(s),
        inStatus(0),
        outStatus(0),
        outStatusTime(millis()),
        outStatusExpiry(outStatusExpiry)
    {}

    void send(const MidiMessage &msg, bool explicitStatus=false)
    {
        if(!msg.validStatus()) return;
        int t=millis();
        if(
                explicitStatus ||
                msg.status != outStatus ||
                ((outStatusTime-t)>outStatusExpiry)
          )
            s.write(msg.status);
        if(msg.type()!=MidiMessage::Type::System)
        {
            outStatus=msg.status;
            outStatusTime=t;
        }
        if(!msg.validData0()) return;
        s.write(msg.data[0]);
        if(!msg.validData1()) return;
        s.write(msg.data[1]);
    }

    void recv(MidiMessage &msg)
    {
        msg = MidiMessage(inStatus);
        if(s.peek() & 0x80)
        {
            // New status byte
            msg.status = s.read();
            // All but system messages update the running status
            if(msg.type() != MidiMessage::Type::System)
                inStatus=msg.status;
        }
        if(msg.status > byte(MidiMessage::SystemType::SongSelect))
            return;
        if(msg.systemType() == MidiMessage::SystemType::SystemExclusive)
        {
            // for now, we just skip the content
            while((s.peek() & 0x80)==0)
                s.read();
            return;
        }
        msg.data[0]=s.read();
        if(
                msg.systemType() == MidiMessage::SystemType::SongSelect ||
                msg.systemType() == MidiMessage::SystemType::TimeCodeQuarterFrame ||
                msg.type() == MidiMessage::Type::ProgramChange ||
                msg.type() == MidiMessage::Type::ChannelAftertouch)
            return;
        msg.data[1]=s.read();
    }
};

inline UMidi& operator<<(UMidi &l, const MidiMessage &r)
{
    l.send(r);
    return l;
}

inline UMidi& operator>>(UMidi &l, MidiMessage &r)
{
    l.recv(r);
    return l;
}
#endif
