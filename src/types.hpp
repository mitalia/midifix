#ifndef TYPES_HPP_INCLUDED
#define TYPES_HPP_INCLUDED
#include <stdint.h>

typedef uint8_t byte;
typedef uint16_t word;
typedef uint32_t dword;

#endif
