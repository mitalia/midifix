#include <Arduino.h>
#include <LiquidCrystal.h>
#include "umidi.hpp"
#include "instrumentnames.hpp"

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

int i=0;

void setup() {
    lcd.begin(16, 2);
}

void loop() {
    lcd.clear();
    lcd.setCursor(0, 0);
    char buf[32];
    strcpy_P(buf, (PGM_P)pgm_read_word(&(instrumentNames[i])));
    lcd.print(buf);
    delay(1000);
    i=(i+1)%128;
}
