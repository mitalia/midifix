#!/usr/bin/env python
import sys

if len(sys.argv)<2 or sys.argv[1]=='--help':
    sys.stderr.write(
"""mkpgmsarr.py - creates an array of PROGMEM strings from the rows of a file

Usage: mkpgmsarr.py arrname <filename >outfile.cpp
    filename       input file name
    arrname        name of the generated array
    outfile.cpp    output file
""")
    sys.exit(1)

print """
#include <avr/pgmspace.h>

namespace
{"""

basename=sys.argv[1]
count=0
for l in sys.stdin:
    s=""
    for c in l[:-1]:
        o=ord(c)
        if o<32 or o>127 or c in "\"\\":
            s+="\\x"+hex(o)[2:]
        else:
            s+=c
    print "    const char %s_%d[] PROGMEM = \"%s\";" % (basename, count, s)
    count+=1

print "}\n"
print "extern PGM_P const %s[] PROGMEM =\n    {" % (basename,)
for i in range(count):
    print "        %s_%d%s" % (basename, i, "," if i<count-1 else "")
print "    };"

